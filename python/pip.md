```
pip list

pip search pkg

查询当前环境可升级的包
pip list --outdated
pip list --outdated --trusted-host pypi.douban.com

查询一个包的详细信息
pip show pkg

下载软件包
pip download  --destination-directory  /local/wheels -r requirements.txt

安装已下载的软件包
pip install --no-index --find-links=local/wheels -r requirements.txt


构建自己的wheel文件
pip install wheel
pip wheel --wheel-dir=/local/wheels -r requirements.txt



指定版本
pip install pkg==2.1.2
pip install pkg<2.1.2
pip install pkg>2.1.2

导出列表
pip freeze > requirements.txt

从列表中安装
pip install -r requirements.txt

确保当前环境软件包的版本
pip install -c constraints.txt


下载非二进制的包
pip download --no-binary=:all: pkg

安装非二进制的包
pip install pkg --no-binary


pip install --user pkg
卸载
pip uninstall pkg
升级
pip install --upgrade pkg


```

