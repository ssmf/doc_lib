### 安装

```
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

bash下安装

```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.bashrc
```













##### zsh



```
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.zshrc
```

### pyenv-virtualenv

```
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
```





echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc



echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.zshrc





## 使用



```
# 检查pyenv版本
pyenv version
# 查看pyenv托管了哪些版本
pyenv versions
# 安装 3.6.6 版本的 python
pyenv install 3.6.6
```

### 依赖

```
ubuntu
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev


centos:
yum install gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel


```

应对错误方案

https://github.com/pyenv/pyenv/wiki





## 虚拟环境

创建

```

pyenv virtualenv 3.6.6 my-env
```

```
pyenv activate my-env
```





自动激活虚拟环境



```
cd ~/test
pyenv local my-env
```



#### 卸载pyenv

rm -rf ~/.pyenv





### 替换下载源

 find ./ -type f  -exec sed -i 's#www.python.org/ftp/#npm.taobao.org/mirrors/#g' {} \;





参考url:

https://www.jianshu.com/p/3e93311fe6cb

