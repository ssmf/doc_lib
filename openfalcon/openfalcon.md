### 告警

|      |                                              | 二进制                                                       | 测试                                                         |
| ---- | -------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 邮件 | https://github.com/open-falcon/mail-provider | https://dl.cactifans.com/open-falcon/falcon-mail-provider.tar.gz | curl http://127.0.0.1:4000/sender/mail -d "tos=lijw@bokecc.com&subject=ooooooo&content=bbbbbbbb" |
| 邮件 | https://github.com/niean/mailsender          | https://github.com/niean/mailsender/releases/download/v0.0.1/tycs-mailsender-0.0.1.tar.gz |                                                              |
| 微信 | https://github.com/Yanjunhui/chat            | git clone https://www.github.com/yanjunhui/chat.git          |                                                              |
| 钉钉 | https://github.com/sdvdxl/falcon-message     | https://github.com/sdvdxl/falcon-message/releases/download/v0.0.2/falcon-message-linux64.zip |                                                              |

 



 文档

http://book.open-falcon.org/zh/index.html

https://github.com/niean/mailsender

​                                     //发送邮件

https://github.com/gaochao1/swcollector/releases

​                    //交换机监控

[相关文章]

```
https://mp.weixin.qq.com/s?__biz=MzAwMDU1MTE1OQ==&mid=405484332&idx=1&sn=3c68a053ab3ef82f27c29a9ba942507b&scene=4
```

\--  

## open-falcon编写的整个脑洞历程

```
https://mp.weixin.qq.com/s?__biz=MjM5OTcxMzE0MQ==&mid=400225178&idx=1&sn=c98609a9b66f84549e41cd421b4df74d&scene=1&srcid=1028BIXhzmVdUytAJoaq6qZE&uin=MjU1NDcwNQ%3D%3D&key=41ecb04b05111003fd5a3f41abeb54744fcdc958bb5498e45bdf4bcfe69fffc5b737a26787c6a03eb9d2e5bf48b9bfeb&devicetype=iMac+MacBookAir7%2C2+OSX+OSX+10.11.1+build(15B42)&version=11020201&lang=zh_CN&pass_ticket=eT8590ZnPRFXfkkZjuk6SKIa3vohkNxGHiHeZrzECIs%3D
```

-- 社区贡献列表

http://book.open-falcon.com/zh/dev/community_resource.html

api

https://openfalcon.docs.apiary.io/#introduction/use-cases/create-an-alearting-rule