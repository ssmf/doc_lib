[redis-shake](https://github.com/alibaba/RedisShake/releases?spm=a2c4g.11186623.2.16.36926e66mLnLY1)工具



云上到云下

```
https://help.aliyun.com/document_detail/66012.html?spm=a2c4g.11186623.6.646.36926e66mLnLY1
```





# 测试工具

memtier_benchmark是Redis Labs推出的命令行工具，可用于在键值存储数据库中生成数据负载并进行压力测试。

```
https://help.aliyun.com/document_detail/100347.html?spm=a2c4g.11186623.6.685.1c8329c4xnNaXw
```

#### 测试命令

```
./memtier_benchmark -s r-***************.redis.rds.aliyuncs.com -p 6379 -a <password> -c 20 -d 32 --threads=10 --ratio=1:1 --test-time=1800 --select-db=10
```

## 测试指标

QPS

Queries Per Second，即数据库每秒处理的请求数。





