[模块]
file
copy
shell
synchronize
fetch
lineinfile

官网:   https://www.ansible.com/
Tower:  http://www.ansible.com/tower
Galaxy: https://galaxy.ansible.com/
Ansible on Github:  https://github.com/ansible/
Tower-cli :  https://github.com/ansible/tower-cli
Ansible on 简书: http://www.jianshu.com/p/c56a88b103f8
官方文档:   http://docs.ansible.com/
中文文档： http://www.ansible.com.cn/  http://ansible-tran.readthedocs.io/
Jinja2 中文文档：   http://docs.jinkan.org/docs/jinja2/
yaml语法：          http://www.yaml.org/
ansible examples ：https://github.com/ansible/ansible-examples
ansible-vim： https://github.com/pearofducks/ansible-vim
可以高亮显示，语法检查
books：  https://www.ansible.com/ebooks



### ansilbe  in vagrant

```
$ vagrant init ansible/tower
$ vagrant up --provider virtualbox
$ vagrant ssh
```

ansible cmd

```
ansible -m setup --tree out/ all
ansible-cmdb out/ > overview.html
```









https://docs.ansible.com/ansible/latest/modules/list_of_network_modules.html

- 执行指定的task
- 变量的使用
- 使用setup的变量, 过滤setup的变量




