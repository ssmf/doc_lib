## 防火墙
## netsh 
## wmic
##  winxp



|          |                        |                                                   |                                                              |      |
| -------- | ---------------------- | ------------------------------------------------- | ------------------------------------------------------------ | ---- |
| OpenLDAP | OpenLDAP for Windows   | https://sourceforge.net/projects/openldapwindows/ | https://nchc.dl.sourceforge.net/project/openldapwindows/openldap-2.4.44/openldap-2.4.44-x64.zip |      |
|          |                        | http://winradius.eu/Index.html                    | http://winradius.eu/downloads/freeradius-server-3.0.18-x64.zip | 收费 |
|          |                        | https://www.kaplansoft.com/tekradius/             | https://www.kaplansoft.com/tekradius/release/TekRADIUS.zip   | 免费 |
| HostAP   |                        |                                                   |                                                              |      |
| Kerberos | Kerberos V for Windows |                                                   |                                                              |      |
| RADIUS   | WinRADIUS              |                                                   |                                                              |      |
|          |                        |                                                   |                                                              |      |
|          |                        |                                                   |                                                              |      |
|          |                        |                                                   |                                                              |      |
|          |                        |                                                   |                                                              |      |



|                   |                                                              |      |
| ----------------- | ------------------------------------------------------------ | ---- |
| SysinternalsSuite | https://download.sysinternals.com/files/SysinternalsSuite.zip |      |
|                   |                                                              |      |
|                   |                                                              |      |





### 安装启动服务



|      |                             |                 |
| ---- | --------------------------- | --------------- |
| nssm | http://www.nssm.cc/download | 将exe封装为服务 |
|      |                             |                 |
|      |                             |                 |



### 日常应用

|                  |            |                                                              |
| ---------------- | ---------- | ------------------------------------------------------------ |
| Q-dir            |            |                                                              |
| myBase Desktop   | 个人知识库 | 树状笔记本                                                   |
| homeftpserver    | ftp服务器  |                                                              |
| kchmviewer       | chm阅读器  | https://sourceforge.net/projects/kchmviewer/files/kchmviewer/ |
| ditto            | 剪切板工具 |                                                              |
| Folder Colorizer | 文件夹变色 |                                                              |
| firefox          |            | https://download-ssl.firefox.com.cn/releases-sha2/full/52.9esr/zh-CN/Firefox-ESR-full-latest.exe |



命令启动应用

|          |            |      |
| -------- | ---------- | ---- |
| eventvwr | 日志查看器 |      |
|          |            |      |
|          |            |      |







### 编程语言

|                     |                                                              |      |
| ------------------- | ------------------------------------------------------------ | ---- |
| lua                 | http://luabinaries.sourceforge.net/                          |      |
| luarocks  (windows) | http://luarocks.github.io/luarocks/releases/                 |      |
| make                | https://sourceforge.net/projects/gnuwin32/files/make/3.81/make-3.81-bin.zip/download?use_mirror=jaist&download= |      |



### 命令行工具

| 命令         |                                                  |          |
| ------------ | ------------------------------------------------ | -------- |
| dig          |                                                  |          |
| awk,grep,sed | https://sourceforge.net/projects/gnuwin32/files/ | gnuwin32 |
| wget         |                                                  |          |
| curl         | https://curl.haxx.se/windows/                    |          |
| jq           | https://stedolan.github.io/jq/download/          |          |
|              |                                                  |          |
|              |                                                  |          |
|              |                                                  |          |
|              |                                                  |          |

### 管理工具

|              |                 |                             |
| ------------ | --------------- | --------------------------- |
| FileTypesMan | www.nirsoft.net | 管理文件类型打开方式,和图标 |
| schtasks     |                 | 计划任务                    |
|              |                 |                             |







### 添加目录到PATH

wmic ENVIRONMENT where "name='path' and username='<system>'" set VariableValue="%path%;d:\tools"



### 工具包和直接下载地址

|          |                                                              | 依赖                                                         |
| :------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| gawk     | https://jaist.dl.sourceforge.net/project/gnuwin32/gawk/3.1.6-1/gawk-3.1.6-1-bin.zip |                                                              |
| grep     | https://jaist.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-bin.zip | https://jaist.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-dep.zip |
| sed      | https://jaist.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-bin.zip | https://jaist.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-dep.zip |
| curl     | https://curl.haxx.se/windows/dl-7.64.1_1/curl-7.64.1_1-win64-mingw.zip |                                                              |
| wget     | http://www.interlog.com/~tcharron/wgetwin-1_5_3_1-binary.zip |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
| wamp     | https://nchc.dl.sourceforge.net/project/wampserver/WampServer%203/Wampee%203.1/Wampee-3.1.0-beta-3.5.7z |                                                              |
| cygwin   | http://fxrj1.ohnomono.com/soft/cygwin.zip                    |                                                              |
| nssm     | http://www.nssm.cc/release/nssm-2.24.zip                     |                                                              |
| openssll | http://slproweb.com/products/Win32OpenSSL.html               |                                                              |
| busybox  | http://frippery.org/busybox/                                 | http://frippery.org/files/busybox/busybox64.exe<br />http://frippery.org/files/busybox/busybox.exe |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |
|          |                                                              |                                                              |

#### DNS

查看dns缓存

ipconfig /displaydns

强制更新缓存

ipconfig /flushdns





优化

|                                |             |      |
| ------------------------------ | ----------- | ---- |
| C:\Windows\Installer           |             |      |
| c:\swapfile.sys                |             |      |
| **并发连接数限制** <u>重要</u> | MaxFreeTcbs |      |


### git
http://npm.taobao.org/mirrors/git-for-windows/v2.22.0.windows.1/
http://npm.taobao.org/mirrors/git-for-windows/v2.22.0.windows.1/Git-2.22.0-64-bit.exell

https://repo.huaweicloud.com/git-for-windows/



# 禁用笔记本自带键盘

```
sc config i8042prt start= disabled
sc config i8042prt start= auto

```





### windows建立快捷方式

Shortcut.exe





### windows 打开用户自动目录

win+r

shell:startup



### 用户自动登录

control userpasswords2



### win10 文件授权 

cacls d:\ /t /p everyone:f

control userpasswords2

https://blog.csdn.net/weixin_30703911/article/details/96076877
icacls * /t /grant:r everyone:f

takeown /f * /A /R 



### 当前哪些用户登录

query user





# choco 工具

choco install docker
choco install telegraf
choco install nmap
choco install mysql
choco install mongodb
choco install winbox
choco install rabbitmq
choco install phantomjs





# 安装ftp服务器



#  字体注册表位置

`
计算机\HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\Fonts
`
### 
runas 

### 
startup 目录拉起的程序子进程可以启动图形



# 激活

slmgr全称：Software License Manager

1. ### slmgr.vbs -dlv  

   > 显示：最为详尽的激活信息，包括：激活ID、安装ID、激活截止日期。

2. ### slmgr.vbs -dli  

   > 显示：操作系统版本、部分产品密钥、许可证状态。

3. ### slmgr.vbs -xpr 
   > 显示：是否永久激活。显示激活截止时间


