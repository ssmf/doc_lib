
```
$dateTimeStr = '20141231T23:59:59'
$format = 'yyyyMMddTHH:mm:ss'
$formatProvider = [Globalization.CultureInfo]::InvariantCulture
[datetime]::ParseExact($dateTimeStr,$format,$formatProvider)
```




# 获取前两天系统的日志
Get-EventLog -LogName System -EntryType Error -After (Get-Date).AddDays(-2)

#   参考
datetime的静态方法static datetime ParseExact
$cstzone = [System.TimeZoneInfo]::FindSystemTimeZoneById("Central Standard Time")
$csttime = [System.TimeZoneInfo]::ConvertTimeFromUtc((Get-Date).ToUniversalTime(), $cstzone)
Get-Date $csttime.AddHours(-1) -f "MM\dd\yyyy HH:mm:ss"

#  测试
$cstzone = [System.TimeZoneInfo]::FindSystemTimeZoneById("China Standard Time")
$csttime = [System.TimeZoneInfo]::ConvertTimeFromUtc((Get-Date).ToUniversalTime(), $cstzone)
$sj = ($(Get-Date $csttime -UFormat "%s" )-split "\.")[0]



$pocketknife=New-Object object
Add-Member -InputObject $pocketknife -Name Color -Value "Red" -MemberType NoteProperty
Add-Member -InputObject $pocketknife -Name Weight -Value "55" -MemberType NoteProperty
$pocketknife | Add-Member NoteProperty Blades 3
$pocketknife | Add-Member NoteProperty Manufacturer ABC
$pocketknife

# 
返回系统运行时间：
function Get-Uptime
{
  $millisec = [Environment]::TickCount
[Timespan]::FromMilliseconds($millisec)
}
# PowerShell 字符串的几种类型判断
```
Function IsBlank ([string]$InputObject) {
  if (($InputObject -eq "") -or ($InputObject -eq $Null)) {
    Return $True
  } else {
    Return $False
  }
}

Function IsInteger ([string]$InputObject) {
  if ($InputObject -match "^\d+$" ) {
    Return $True
  } else {
    Return $False
  }
}

Function IsFloat ([string]$InputObject) {
  if ($InputObject -match "^\d+\.\d+$") {
    Return $True
  } else {
    Return $False
  }
}

Function IsIPAddr ([string]$InputObject) {
  if ($InputObject -match "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$") {
    Foreach ($Local:str in $InputObject.split(".")) {
      if (([int16]$str -gt 255) -or (($str -match "^0") -and ($str -ne "0"))) {
        #IP任意一段大于255或(以0开头但不等于0)则无效
        Return $False
      }
    }
    if ( [int16]$InputObject.split(".")[0] -eq 0 ) {
      #IP首位等于0则无效
      Return $False
    }
      Return $True
  } else {
    #IP不符合四段3位数值格式则无效
    Return $False
  }
}


Function IsNetmask([string]$InputObject) {
  if ($InputObject -match "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$") {
    #将子网掩码转换为二进制字符串，不足8位的在左侧填0
    $Mask = -join ($InputObject.Split('.') | ForEach-Object {[System.Convert]::ToString($_,2).PadLeft(8,'0')})
    #判断是否连续1开头，连续0结尾
    if (($Mask -match '^1+0+$') -and ($Mask.Length -le 32)) {
      Return $True
    } else {
      Return $False
    }
  } else {
    Return $False #不符合IP的四段3位数字格式
  }
}
```
# 多为数组
$array3D=New-Object 'int[,,]' 3,4,5
访问多维数组关键是获取数组的维数和每一维对应的长度
$array3D.GetLength(0)
3

$array3D.GetLength(1)
4

$array3D.GetLength(2)
5
```
#声明
$array3D=New-Object 'int[,,]' 3,4,5
#赋值
for($i=0;$i -lt $array3D.GetLength(0);$i++)
{
 for($j=0;$j -lt $array3D.GetLength(1);$j++)
 {
  for($k=0;$k -lt $array3D.GetLength(2);$k++)
  {
   $array3D[$i,$j,$k]=$i+1
  }
 }
}
#打印
for($i=0;$i -lt $array3D.GetLength(0);$i++)
{
 for($j=0;$j -lt $array3D.GetLength(1);$j++)
 {
  Write-Host ("`t"*$i) -NoNewline
  for($k=0;$k -lt $array3D.GetLength(2);$k++)
  {
    Write-Host $array3D[$i,$j,$k] -NoNewline
    Write-Host "`t" -NoNewline
  }
  Write-Host "`n" -NoNewline
 }
 Write-Host
}
```
#  json
```
$json = @"
{
    "ServerName": "$env:ComputerName",
    "UserName": "$env:UserName",
    "BIOS": {
         "Manufacturer" : "$((Get-WmiObject -Class Win32_BIOS).Manufacturer)",
         "Version" : "$((Get-WmiObject -Class Win32_BIOS).Version)",
         "Serial" : "$((Get-WmiObject -Class Win32_BIOS).SerialNumber)"
         },
    "OS" : "$([Environment]::OSVersion.VersionString)"
 }
"@
 
$info = ConvertFrom-Json -InputObject $json
 
$info.ServerName
$info.BIOS.Version
$info.OS
#转回JSON格式
$info | ConvertTo-Json
```
# Powershell 开启远程桌面
```powershell
function Set-RemoteDesktop
{
  while($InNumber -ne 6)
  {
  Write-Host " ##########################################################" -ForegroundColor Green
  Write-Host " # 1、自定义远程桌面端口；                                #"
  Write-Host " # 2、开启远程桌面；                                      #"
  Write-Host " # 3、开启防火墙远程桌面出入端口；                        #"
  Write-Host " # 4、恢复系统默认的远程桌面端口；                        #"
  Write-Host " # 5、禁用远程桌面；                                      #"
  Write-Host " # 6、退出菜单；                                          #"
  Write-Host " # ########################################################" -ForegroundColor Green
  $InNumber = Read-Host "输入编号进行操作"

      switch ($InNumber)
      {
          1 {
              $PortNumber = Read-Host "请输入修改远程桌面端口号"
              Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -Name 'PortNumber' -Value $PortNumber
              $PortResult = Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -Name 'PortNumber'
              if($PortResult.PortNumber -eq $PortNumber)
                  {
                    Write-Host "已经成功修改端口为$PortNumber" -ForegroundColor Green
                  }
                  else
                  {
                    Write-Error "端口修改失败，请重试...."
                  }
          }
          2 {
              Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server' -Name 'fDenyTSConnections' -Value 0
              Write-Host 正在重启 Remote Desktop Services ... -ForegroundColor DarkYellow
              Set-Service TermService -StartupType Automatic -Status Running -PassThru             
          }
          3 {
              $Check = New-NetFirewallRule -DisplayName "Allow RDP" -Direction Inbound -Protocol TCP -LocalPort $PortNumber -Action Allow
              if($Check.PrimaryStatus -eq 'OK')
                {
                    Write-Host "成功设置防火墙策略" -ForegroundColor Green
                }
                else
                {
                    Write-Error "防火墙策略设置失败,请重试..."
                }

          }
          4 {
              Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -Name 'PortNumber' -Value 3389             
              $PortResult = Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -Name 'PortNumber'
              if($PortResult.PortNumber -eq 3389)
                  {
                    Write-Host "已经成功恢复系统默认设置" -ForegroundColor Green
                  }
                  else
                  {
                    Write-Error "恢复失败，请重试...."
                  }
     
          }
          5 {
           
              Write-Host 正在停止 Remote Desktop Services ... -ForegroundColor DarkYellow
              Set-Service TermService -StartupType Disabled -Status Stopped -PassThru
              Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server' -Name 'fDenyTSConnections' -Value 1
          }
          6 {}
          Default { Write-Error "请输入1-5编号"}
      }
    Start-Sleep 2
    Invoke-Command {cls}
    }
}
```
#  powershell logging
#  管理员权限运行 powershell
Start-Process powershell -Verb runAs 
#  split 切割
#  枚举 
```
[Enum]::GetNames([DayOfWeek])  //英文  星期
[Globalization.DatetimeFormatInfo]::CurrentInfo.MonthNames  //中文 月
```
# 发送邮件

#  System.String 转为  浮点数
$a = "9.9"
$b = [convert]::ToDouble($a)

#  powershell get请求 / post请求
实例1
```
$Body = @{
    User = 'jdoe'
    password = 'P@S$w0rd!'
}
$LoginResponse = Invoke-WebRequest 'https://www.contoso.com/login/' -SessionVariable 'Session' -Body $Body -Method 'POST'

$Session

$ProfileResponse = Invoke-WebRequest 'https://www.contoso.com/profile/' -WebSession $Session

$ProfileResponse
```
实例 二
```
$webRequest = [System.Net.WebRequest]::Create($target)
#$encodedContent = [System.Text.Encoding]::UTF8.GetBytes($content)
$encodedContent = $Process_Poly
$webRequest.Method = "POST"

write-host "UTF8 Encoded Http Content: $content"

$webRequest.ContentType = "application/json"
$webRequest.ContentLength = $encodedContent.length
$requestStream = $webRequest.GetRequestStream()
$requestStream.Write($encodedContent,0,$encodedContent.length)
$requestStream.Close()
```
#  powershell 计划任务
Get-Command -Module PSScheduledJob
```
https://www.pstips.net/about-scheduledjob.html#添加计划工作
```







