### 修改IP地址addr和子网掩码mask：

netsh interface ip set address name="本地连接" source=static addr=192.168.0.106 mask=255.255.255.0

### 修改默认网关gateway

netsh interface ip set address name="本地连接" gateway=192.168.0.1 gwmetric=0

### 修改首选(PRIMARY)的DNS

netsh interface ip set dns name="本地连接" source=static addr=202.96.128.66 register=PRIMARY  







# mtu

## 查看mtu

#### `netsh interface ipv4 show subinterfaces` 



## 修改mtu

netsh interface ipv4 set subinterface "连接名" mtu=值 store=persistent（如：netsh interface ipv4 set subinterface "WLAN" mtu=1492 store=persistent）