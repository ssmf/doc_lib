##### 新建目录

C:\etc\openvpn

将pc.ovpn和openvpn.pass 拷贝到目录



##### 下载nssm , 将nssm.exe 放到c:\windows\

http://www.nssm.cc/download



##### 安装服务(openvpnpc为服务名)

cmd--> nssm install openvpnpc

|                    |                                                |      |
| ------------------ | ---------------------------------------------- | ---- |
| application Path:  | C:\Program Files (x86)\OpenVPN\bin\openvpn.exe |      |
| startup directory: | C:\etc\openvpn                                 |      |
| arguments:         | pc.ovpn                                        |      |

##### 启动服务

net start openvpnpc





