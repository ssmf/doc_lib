



#### 操作硬盘离线,在线

Set state to offline

```
MegaCli -PDOffline -PhysDrv [E:S] -aN 
```

Set state to online

```
MegaCli -PDOnline -PhysDrv [E:S] -aN 
```



#### 热备盘设置



```
MegaCli -PDHSP {-Set [-Dedicated [-ArrayN|-Array0,1,2...]] [-EnclAffinity] [-nonRevertible]}  -Rmv -PhysDrv[E0:S0,E1:S1,...] -aN|-a0,1,2|-aALL
```

全局热备

```
MegaCli -PDHSP -Set -PhysDrv[14:1] -a0
```

局部热备

```
MegaCli -PDHSP -Set -Dedicated -Array2 -PhysDrv[14:1] -a0
```

删除热备

```
megacli  -PDHSP -rmv  -PhysDrv [32:11] -a0
```



```
https://gist.github.com/dubcl/ee3c85d561cc39cc4096276b728b1502
```

#### 查看

查看重建进度

```
megacli   -PDRbld -ShowProg -PhysDrv [32:11] -a0
```

