`
echo_supervisord_conf > /etc/supervisord.conf
`



-- catalina.sh 中添加
JAVA_HOME="/usr/java/"
CLASSPATH="$JAVA_HOME/lib/dt.jar:$JAVE_HOME/lib/tools.jar"

-- tomcat.ini
[program:tomcat]
command=/root/tools/apache-tomcat-7.0.70/bin/catalina.sh run
stdout_logfile=/root/tools/apache-tomcat-7.0.70/logs/catalina.out
autostart=true
autorestart=true
startsecs=5
priority=1
stopasgroup=true
killasgroup=true

-- 需要将docker的二进制文件存放到系统默认的path目录中, 比如/bin/
-- 或 http://supervisord.org/subprocess.html#subprocess-environment  
[program:dockerd]
autorestart=True
autostart=True
redirect_stderr=True
command=/usr/local/docker-18.09.6/dockerd
user=root
directory=/usr/local/docker-18.09.6/
log_stdout=true
log_stderr=true
stdout_logfile_maxbytes = 20MB
stdout_logfile_backups = 20
stdout_logfile = /tmp/dockerd.log
