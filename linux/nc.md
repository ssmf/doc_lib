# nc测试服务器UDP端口

```
-w, --wait <time>：Connect timeout；连接超时时长。例如设为10秒，如果10秒后未连接成功则停止发起连接请求并返回失败信息
-v, --verbose：Set verbosity level
-u, --udp：Use UDP instead of default TCP
-n, --nodns：Do not resolve hostnames via DNS
-z：zero-I/O mode [used for scanning]；如果端口无回应的时候加上；如果有回应-z参数需去掉（扫描端口是否打开）
```

**nc -unvz -w 1 192.168.10.88 2012**



# nc帮助



```
nc [-46DdhklnrStUuvzC] [-i interval] [-p source_port] 
[-s source_ip_address] [-T ToS] [-w timeout] [-X proxy_version] 
[-x proxy_address[:port]] [hostname] [port[s]] 
Command Summary: 
-4 Use IPv4 
-6 Use IPv6 
-D Enable the debug socket option 
-d Detach from stdin 
-h This help text 
-i secs Delay interval for lines sent, ports scanned 
-k Keep inbound sockets open for multiple connects 
-l Listen mode, for inbound connects 
-n Suppress name/port resolutions 
-p port Specify local port for remote connects 
-r Randomize remote ports 
-S Enable the TCP MD5 signature option 
-s addr Local source address 
-T ToS Set IP Type of Service 
-C Send CRLF as line-ending 
-t Answer TELNET negotiation 
-U Use UNIX domain socket 
-u UDP mode 
-v Verbose 
-w secs Timeout for connects and final net reads 
-X proto Proxy protocol: “4”, “5” (SOCKS) or “connect” 
-x addr[:port] Specify proxy address and port 
-z Zero-I/O mode [used for scanning] 
Port numbers can be individual or ranges: lo-hi [inclusive]

```


