
docker rm -f cowrie

#目录挂载没有到指定位置  ,但是在/var/lib/docker/volumes/var/_data
docker run -dit --name cowrie -v var:/cowrie/cowrie-git/var -v etc:/cowrie/cowrie-git/etc   -p 2232:2222 -p 2233:2223 cowrie/cowrie
#使用绝对路径,还是不行
docker run -dit --name cowrie -v /opt/cowrie/var:/cowrie/cowrie-git/var -v /opt/cowrie/etc:/cowrie/cowrie-git/etc   -p 2232:2222 -p 2233:2223 cowrie/cowrie

#22端口
docker run -dit --name cowrie -v var:/cowrie/cowrie-git/var -v etc:/cowrie/cowrie-git/etc   -p 22:2222 -p 23:2223 cowrie/cowrie


docker run -dit --name cowrie   -p 22:2222 -p 23:2223 cowrie/cowrie


#只能到这看日志
/var/lib/docker/volumes/var/_data
docker inspect cowrie
