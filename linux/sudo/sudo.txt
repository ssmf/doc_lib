user        ALL=(ALL)       NOPASSWD: ALL
ulimit  -n  1048578 
Operation not permitted



nagios ALL=(ALL) NOPASSWD:/usr/sbin/megasasctl,/usr/sbin/megacli,/sbin/mii-tool,/bin/rm
zabbix ALL=(ALL) NOPASSWD:/usr/sbin/megasasctl,/usr/sbin/megacli,/sbin/mii-tool,/sbin/ethtool,/usr/bin/lsof
phenix ALL=(ALL) NOPASSWD:/sbin/mii-tool,/var/www/dream/falcon-agents/control