



./wrap /sbin/tc qdisc del dev   em1 root 
./wrap /sbin/tc qdisc add dev   em1 root handle 1: htb default 10
./wrap /sbin/tc class add dev   em1 parent 1: classid 1:1 htb rate $bandwidth ceil $bandwidth
./wrap /sbin/tc class add dev   em1 parent 1:1 classid 1:10 htb rate $bandwidth ceil $bandwidth 
./wrap /sbin/tc class add dev   em1 parent 1:1 classid 1:20 htb rate $ipbandwidth ceil $ipbandwidth
./wrap /sbin/tc qdisc add dev   em1 parent 1:10 sfq perturb 10
./wrap /sbin/tc qdisc add dev   em1 parent 1:20 sfq perturb 10
./wrap /sbin/tc filter add dev  em1  protocol ip parent 1: prio 50 u32 match ip dst $oipaddr classid 1:20
./wrap /sbin/tc filter add dev  em1  protocol ip parent 1: prio 50 u32 match ip src $oipaddr classid 1:20