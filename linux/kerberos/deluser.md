# del_user.exp

```
#!/usr/bin/expect -f 

set timeout 10

set user [lindex $argv 0]

spawn  kadmin.local -q "delprinc $user@BOKECC.COM"

expect "(yes/no)" 
send  "yes\r"
expect eof

```

