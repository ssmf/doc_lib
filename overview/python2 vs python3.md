

tkinter



|      | python2                               | python3                                        |
| ---- | ------------------------------------- | ---------------------------------------------- |
|      | import Tkinter<br>import tkMessageBox | from tkinter import messagebox as tkMessageBox |
|      |                                       |                                                |
|      |                                       |                                                |

```python
tkMessageBox.askokcancel('askokcancel', 'hello', parent=root_window,)
tkMessageBox.askquestion('askquestion', 'hello', parent=root_window)
tkMessageBox.askretrycancel('askretrycancel', 'hello', parent=root_window)
tkMessageBox.askyesno('askyesno', 'hello', parent=root_window)
tkMessageBox.showinfo('showinfo', 'hello', parent=root_window)
tkMessageBox.showerror('showerror', 'hello', parent=root_window)
tkMessageBox.showwarning('showwarning', 'hello', parent=root_window)
```

