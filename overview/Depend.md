|            | ubuntu                                  | centos                 |           |
| ---------- | --------------------------------------- | ---------------------- | --------- |
| openssl    | openssl libssl-dev                      | openssl openssl-devel  |           |
| pcre       | libpcre3 libpcre3-dev                   |                        |           |
| zlib       | zlib1g-dev                              |                        |           |
| libxml2    | libxml2-dev                             |                        |           |
| libxslt    | libxslt-dev                             |                        |           |
| GD         | libgd-dev                               |                        | for nginx |
| GeoIP      | geoip-bin  geoip-database  libgeoip-dev |                        |           |
|            | libcurl4-openssl-dev                    | libcurl  libcurl-devel | curl.h    |
|            | python-dev                              | python-devel           | Python.h  |
|            | libzmq-dev                              |                        | zmq.h     |
|            | libpcap-dev                             | libpcap-devel libpcap  | pcap.h    |
|            | libncurses5-dev                         | ncurses-devel          | ncurses.h |
| cyrus-sasl | libsasl2-dev                            |                        |           |
| libtool    | libtool                                 |                        |           |
|            | libwrap0-dev                            |                        | tcpd.h    |
| apxs       | httpd-devel                             |                        |           |
