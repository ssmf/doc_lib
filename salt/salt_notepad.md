|                                                  |                       |      |
| ------------------------------------------------ | --------------------- | ---- |
| salt 'mq1' sys.list_modules 's*'                 | 列出所以以s开头的模块 |      |
| salt 'mq1' sys.list_modules                      |                       |      |
| salt 'mq1' sys.list_functions pkg                |                       |      |
| salt 'mq1' sys.doc pkg.install                   | 模块帮助文档          |      |
| salt '\*' sys.list_state_functions pkg/cmd/state |                       |      |
| salt 'mq1' sys.list_functions 'sys.list_*'       |                       |      |
|                                                  |                       |      |
|                                                  |                       |      |
|                                                  |                       |      |

