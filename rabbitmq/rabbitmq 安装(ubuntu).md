### 下载rabbitmq3.6.16

<https://github.com/rabbitmq/rabbitmq-server/releases/download/rabbitmq_v3_6_16/rabbitmq-server_3.6.16-1_all.deb>



### 安装依赖

apt-get install socat  -y



安装rabbitmq

dpkg –i rabbitmq-server_3.6.16-1_all.deb 



启用guest(配置1) /etc/rabbitmq/rabbitmq.config

[{rabbit, [{loopback_users, []}]}].

#### 内存可用率(配置2)

[{rabbit, [{loopback_users, []}, {hipe_compile, true}, {vm_memory_high_watermark,0.8}]}].



启用管理插件

rabbitmq-plugins enable rabbitmq_management

### 重启rabbitmq

systemctl restart rabbitmq-server

### 集群配置

\#拷贝mq1 的 /var/lib/rabbitmq/.erlang.cookie 到mq2

rabbitmqctl  stop_app

rabbitmqctl  reset

rabbitmqctl join_cluster rabbit@mq1

### 修改文件打开数

1. 修改系统文件/etc/security/limits.conf   
```
*               soft    nofile          65535
* 				hard    nofile          65535
```
2. 修改rabbitmq-env.conf
```
ulimit -S -n 4096
```
3. 重启rabbitmq
4. 查看    rabbitmqctl status  ----->  total_limit



修改nodename

vim /etc/rabbitmq/rabbitmq-env.conf

```
NODENAME=rabbitmq162
```





### 停止rabbitmq时很慢

