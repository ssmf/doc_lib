#!/usr/bin/python
#-*- coding:utf-8 -*-

import sys
import urllib2
import base64
import json
import time
import socket
import requests
import os
from copy import deepcopy

base64string = base64.b64encode('guest:guest')
header = {"Authorization": "Basic %s" % base64string}
url = "http://127.0.0.1:15672/api/overview"
node_url = "http://127.0.0.1:15672/api/nodes"
p = []
hostname = socket.gethostname()
timestamp = int(time.time())
v = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 0: 'OK', 1: 'WARNING', 2: 'CRITICAL'}
data = {
    "endpoint": hostname,
    "tags": "",
    "timestamp": timestamp,
    "metric": "service.rabbitmqmgt.alive",
    "value": v['OK'],
    "counterType": "GAUGE",
    "step": 60
}


try:
    r = requests.get(url, headers=header, timeout=3)
    if r.status_code == 200:
        req_data = r.json()
        mon_list = ["channels", "connections", "consumers", "exchanges", "queues"]
        for mon in mon_list:
            mon_value = req_data['object_totals'][mon]
            d = deepcopy(data)
            d['metric'] = "rabbitmq_%s" % mon
            d['value'] = mon_value
            p.append(d)
    else:
        raise Exception("access error!")
    r = requests.get(node_url, headers=header, timeout=3)
    if r.status_code == 200:
        req_data = r.json()
        nodes_num=len(req_data)
        for i in range(nodes_num):
            for mon in ("sockets_used", "fd_used", "mem_used", \
                        "fd_total", "mem_limit", "sockets_total",\
                        "io_read_avg_time", "io_seek_avg_time", \
                        "io_write_avg_time", "io_sync_avg_time"):
                d = deepcopy(data)
                d['metric'] = "rabbitmq_%s" % mon
                d['value'] = req_data[i][mon]
                d['tags'] = "node=%s" % req_data[i]['name']
                p.append(d)
            fd_used=req_data[i]['fd_used']
            fd_total = req_data[i]['fd_total']

            mem_used = req_data[i]['mem_used']
            mem_limit = req_data[i]['mem_limit']

            sockets_used = req_data[i]['sockets_used']
            sockets_total = req_data[i]['sockets_total']

            # 监控文件描述符 使用率
            d = deepcopy(data)
            d['metric'] = "rabbitmq_%s" % "fd_usage"
            d['value'] = float(format(float(fd_used)*100/float(fd_total),'.2f'))
            d['tags'] = "node=%s" % req_data[i]['name']
            p.append(d)
            # 监控内存 使用率
            d = deepcopy(data)
            d['metric'] = "rabbitmq_%s" % "mem_usage"
            d['value'] = float(format(float(mem_used)*100/float(mem_limit),'.2f'))
            d['tags'] = "node=%s" % req_data[i]['name']
            p.append(d)
            # sockets 使用率
            d = deepcopy(data)
            d['metric'] = "rabbitmq_%s" % "socket_usage"
            d['value'] = float(format(float(sockets_used)*100/float(sockets_total),'.2f'))
            d['tags'] = "node=%s" % req_data[i]['name']
            p.append(d)
    else:
        raise Exception("access error!")

except Exception:
    data["value"] = -1
p.append(data)
print json.dumps(p)