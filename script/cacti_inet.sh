#!/bin/bash/
#Author :shic@bokecc.com
#Date   :2012-1-7
#--------------main()----------------
#CACTI变量初始化
#PHP="/var/www/webapps/www/php5.2/bin/php -q"
shell_dir=$(cd `dirname $0`; pwd)
logfile="$shell_dir/run.log"

Log(){
	sj=`date +%F_%T`
	echo  $sj $* >> $logfile
}
#无法新加主机时,snmp获取网卡的 关键字
network_card_key='Broadcom|Intel'
#内网图象的关键字 Intranet
PHP="/var/www/dream/php-cgi/bin/php -q"
DEVICE="/var/www/webapps/www/cacti-new/cli/add_device.php"
GRAPHS="/var/www/webapps/www/cacti-new/cli/add_graphs.php"
TREE="/var/www/webapps/www/cacti-new/cli/add_tree.php"
GRAPHS_TEMPLATE="/var/www/webapps/www/cacti-new/cli/add_graph_template.php"
PERMS="/var/www/webapps/www/cacti-new/cli/add_perms.php"
UPDATE="/var/www/webapps/www/cacti-new/cli/host_update_template.php"
cacticli_path="/var/www/webapps/www/cacti-new/cli/"
cacti_path="/var/www/webapps/www/cacti-new/rra"
ips="/tmp/CactiList"
#------------------------------------
#mysql_user="omd"
#mysql_passwd="omd2012"
#mysql_ip="10.12.70.137"
#mysql_ip="127.0.0.1"
#mysql_port="3346"
#mysql_bin="/var/www/dream/mysql-azurebreeze/bin/mysql -u${mysql_user} -p${mysql_passwd} -h${mysql_ip} -P${mysql_port} -e"
#${mysql_bin} "select ip1,name from azurebreeze.server where monitor_status = '1' and scrap_id != '1' and name != 'OFF' and name != '' order by ip1;" | egrep -v "name|ip1" |egrep -v "69.28.62.66|45.113.32.215|128.1.78.82|118.190.84.40"  >${Cacti_Server}
#${mysql_bin} "select ip1,name from azurebreeze.server where name != 'OFF' and name != '' order by ip1;" | egrep -v "name|ip1" >${Cacti_Server}
#awk '{print $2}' "${Cacti_Server}" | sed -ie "s/\(vdn\)\([0-9]*\)-\(.*\)-\([0-9]*\)/\1\2-\3-\4.\1\2/g" "${Cacti_Server}"

#-----------------------------------
#固定的Caccti变量值
host_tid=`${PHP} ${DEVICE} --list-host-templates | grep "BokeCC" | awk '{print $1}'`

cd ${cacticli_path}

#1.保证Cacti主机列表和Mysql列表一致
update_cactilist()
{
        cacti_hostname=`${PHP} ${TREE} --list-hosts | grep -w ${ip} | awk '{print $4}'`
        hostsid=`${PHP} ${TREE} --list-hosts | grep -w $ip | awk '{print $1}'`
		if [[ "x${cacti_hostname}" != "x${hostname}" ]]
		then
                /var/www/dream/mysql-cacti/bin/mysql -uroot -p111 -e "update cacti.host set description='${hostname}' where hostname='${ip}';"
                ${PHP} ${UPDATE} --host-id=$hostsid --host-template=${host_tid} -d > /dev/null
        fi
}

#2.Add Device(添加设备)
add_device()
{
        ${PHP} ${DEVICE} --description=$hostname --ip=$ip --template=${host_tid} --avail=snmp --version=2 --community=public  --port=161 --timeout=1000 --max_oids=10 > /dev/null
}

#3.New Graph(添加流量图)
new_graph()
{       
	# 0,  新建图形正常
        # 1,  网卡名字异常
        # 2,  新建图形失败
        intger=`snmpwalk -v 2c -c public -t 0.5 $ip 1.3.6.1.2.1.4.20.1.2|grep -Po "\d+.*"|grep  '^10\.'|awk '{print $4}'`
        interface=`snmpwalk -v 2c -c public -t 0.5  $ip   1.3.6.1.2.1.31.1.1.1.1|grep -Po "\d+.*"|awk '$1=="'"${intger}"'"  {print $4}'|egrep -v \"$network_card_key\"`
	if [ x"" == x"$interface" ] ; then
		Log "$LINENO $hostname find network disagree  name "
		echo 1
	else
        	inet_snmp_qid=`${PHP} ${GRAPHS} --list-snmp-queries |grep "SNMP - Interface Statistics" |awk '{print $1}'`
        	inet_snmp_qtid=`${PHP} ${GRAPHS} --snmp-query-id=${inet_snmp_qid} --list-query-types | grep "In/Out Bits (64-bit Counters)" | awk '{print $1}'`
        	${PHP} ${GRAPHS} --graph-title="|host_description| - Traffic - $interface - Intranet" --graph-type=ds --graph-template-id=${1} --host-id=${hostid} --snmp-query-id=${inet_snmp_qid} --snmp-query-type-id=${inet_snmp_qtid} --snmp-field=ifDescr --snmp-value=$interface > /dev/null   && echo 0 || echo 2
	fi
}
	

#4.Update Grahp/Device(更新设备和流量图)
update_dg()
{
        ${PHP} ${UPDATE} --host-id=$hostid --host-template=${host_tid} -d > /dev/null 
}

#5.Add Trees(添加树仅限VDN节点)
add_tree()
{
        ${PHP} $TREE --list-trees |grep "${tree_name}" > /dev/null
        if [ $? -eq "0" ];then
                echo "The ${tree_name} tree already exists."
        else
                # 建立一级树
                ${PHP} $TREE  --type=tree --name="$tree_name" --sort-method=natural > /dev/null
                # 建立数下面的三个次级目录(CACHE-L7-STORAGE)
                tree_id=`${PHP} $TREE  --list-trees | grep "${tree_name}" | awk '{print $1}'`
                nodearray=(${tree_name}-L7 ${tree_name}-Cache ${tree_name}-Storage)
                numbers=`echo ${#nodearray[*]}`
                for ((m=0;m<$numbers;m++))
                do
					allname=`echo "${nodearray[$m]}"`
                    ${PHP} $TREE  --type=node --node-type=header --tree-id=${tree_id}  --name=$allname
                done
        fi
}

#6.Change Graph(更改设备图最大值)
change_gr()
{
        while read line;do
                hostname2=`echo ${line} | awk '{print $2}' | tr A-Z a-z | sed 's/.bokecc.com//g'`
                for server_traffic in `ls ${cacti_path}/${hostname2}*`; do
                        rrdtool tune "${server_traffic}" --maximum traffic_in:1.0000000000e+13
                        rrdtool tune "${server_traffic}" --maximum traffic_out:1.0000000000e+13
                done
                echo "Updating This ${server_traffic}"
        done<${ips}
}

######
FFID(){         
        FID=`${PHP} ${PERMS} --list-graphs --host-id=${sc} | grep Intranet`
		if [ x"" == x"$FID" ] ;then
		    Log "$LINENO sc= $sc| fid not found"
		else
            FFFID=`echo $FID | awk '{print $1}'`
            ${PHP} ${TREE} --type=node --node-type=graph --tree-id=${VDNID} --parent-node=$1 --graph-id=${FFFID}
		    Log "$LINENO: add VDNID:$VDNID|\$1= $1 |sc= $sc |FID= $FID| FFFID=$FFFID"
		fi
}

#7.更新设备图至树下
update_allg()
{
        VDNID=`${PHP} ${TREE} --list-trees | grep -i "IntranetView" | awk '{print $1}'`
        VDN33ID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "vdn33" | awk '{print $2}'`
        VDN34ID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "vdn34" | awk '{print $2}'`
        VDN42ID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "vdn42" | awk '{print $2}'`
        VDN44ID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "vdn44" | awk '{print $2}'`
        VDN51ID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "vdn51" | awk '{print $2}'`
        VDNEID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "cc-e"  | awk '{print $2}'`
        VDNHID=`${PHP} ${TREE} --list-nodes --tree-id=${VDNID} | grep -i "header" | grep -i "cc-h"  | awk '{print $2}'`

	    Log "$LINENO VID: $VID| VDN33ID: $VDN33ID| VDN34ID $VDN34ID | VDN42ID $VDN42ID | VDN44ID $VDN44ID "
        cat $ips | while read hostname;do
            what_node=`echo $hostname|  awk -F '-' '{print $1}'`
			sc=`${PHP} ${GRAPHS} --list-hosts|grep $hostname |awk '{print $1}'`	
			Log "$LINENO hostname:$hostname |sc: $sc"
			
             case $what_node in
                "vdn33" ) FFID ${VDN33ID} ;;
                "vdn34") FFID ${VDN34ID}  ;;
                "vdn42") FFID ${VDN42ID}  ;;
                "vdn44") FFID ${VDN44ID}  ;;
                "vdn51") FFID ${VDN51ID}  ;;
                "vdne") FFID ${VDNEID}  ;;
                "vdnh") FFID ${VDNEID}  ;;
              esac
        done
}

#8.判断为何种类型的设备更换其相应的图像模版
judge()
{
        #vdn_gr=`echo $hostname | grep -i "vdn"`
        #if [ $? = 0 ];then
        #        inet_graph_tid=`${PHP} ${GRAPHS_TEMPLATE} --list-graph-templates|grep "VDN服务器"| awk '{print $1}'`
        #fi
        guangh_tiant_gr=`echo $hostname | egrep -i "cc-b|cc-c|encoder|cc-e|cc-f|vdn"`
        if [ $? = 0 ];then
                inet_graph_tid=`${PHP} ${GRAPHS_TEMPLATE} --list-graph-templates|grep "TRAFFIC-core-app-auto-negotiation"| awk '{print $1}'`
        fi
        tongn_gr=`echo $hostname | grep -i "cc-a"`
        if [ $? = 0 ];then
                inet_graph_tid=`${PHP} ${GRAPHS_TEMPLATE} --list-graph-templates|grep "铜牛转码核心机房"| awk '{print $1}'`
        fi
}

#11.尝试修复SNMP不通的机器
repair()
{
        while read line;do
                false_ip=`echo $line | awk '{print $1}'`
                real_ip=`/usr/bin/snmpwalk -v 2c -c public -t 0.5 ${false_ip} sysContact  | awk '{print $4}' ` >/dev/null
                if [ "${real_ip}" != "Feng_Hao" ];then
                        wget http://cacti.bokecc.com/nagios/repair_snmp.sh -O ${access_log}/repair_snmp.sh
                        scp -P 12 -i ${access_log}/list -o StrictHostKeyChecking=no ${access_log}/repair_snmp.sh root@${false_ip}:/tmp/
                        ssh -n -p 12 -i ${access_log}/list -o StrictHostKeyChecking=no root@${false_ip} "bash /tmp/repair_snmp.sh" >/dev/null 2>&1
                        echo "Reair This ${line}Dev....."
                        rm ${access_log}/repair_snmp.sh
                fi
        done<${ips}
}

#10.start
#hostname=$1
addinet(){
while read hostname ; do 
read -t 10 -p $hostname 
ip=`${PHP} ${GRAPHS}  --list-hosts |grep $hostname |awk '{print $2}'`
#0.保证现有cacti列表主机名与mysql一致
real_ip=`/usr/bin/snmpwalk -v 2c -c public -t 0.5 $ip sysContact  | awk '{print $4}' ` >/dev/null
#1.判断snmp连通性
if [ "${real_ip}" = "Feng_Hao" -o "${real_ip}" = "Shi_Chen" ];then
    #2.判断此IP是否在Cacti列表中
	hostid=`${PHP} ${GRAPHS}  --list-hosts | grep -w "$ip" |awk '{print $1}'`
	network_card_key_num=`${PHP} ${GRAPHS} --host-id=${hostid} --snmp-field=ifDescr --list-snmp-values|egrep "$network_card_key"|wc -l`

	if [ $network_card_key_num -gt 0 ] ; then
			Log "$LINENO hostname: $hostname|key_num: $network_card_key_num| 发现异常网卡, 请手动添加图形"
		else
			if [  "x${hostid}" = "x" ];then
				add_device
				judge
				if_eth0=`${PHP} ${GRAPHS} --host-id=${hostid} --snmp-field=ifDescr --list-snmp-values | grep "$interface"`
				#if_bond0=`${PHP} ${GRAPHS} --host-id=${hostid} --snmp-field=ifDescr --list-snmp-values | grep "bond0"`
				if [ "x${if_eth0}" != "x"  ];then
					new_graph ${inet_graph_tid} ${if_eth0}	
					$? = 1  && Log "$LINENO: add graph error "  
				fi
				graphid=`${PHP} $TREE --list-graphs --host-id=$hostid | grep "[0-9]" | awk '{print $1}'`
				#4.更新新添加设备的源
				update_dg
				echo "Adding This -- $ip $hostname"
			else
				# 检查是否光有设备没有图
				hostid=`${PHP} ${GRAPHS}  --list-hosts | grep -w "${ip}" | awk '{print $1}'`
				#是否有图
				data=`${PHP} ${PERMS} --list-graphs --host-id=${hostid} | grep  Intranet`
				host_tid=`${PHP} ${DEVICE} --list-host-templates | grep "BokeCC" | awk '{print $1}'`
				intger=`snmpwalk -v 2c -c public -t 0.5 $ip 1.3.6.1.2.1.4.20.1.2|grep -Po "\d+.*"|grep  '^10\.'|awk '{print $4}'`
					interface=`snmpwalk -v 2c -c public -t 0.5  $ip   1.3.6.1.2.1.31.1.1.1.1|grep -Po "\d+.*"|awk '$1=="'"${intger}"'"  {print $4}'`
					Log "$LINENO: hostid= $hostid| data= $data| host_tid= $host_tid| intger= $intger| interface=$interface |"
					if [ "x$data" = "x" ];then
						#判断为何种类型的设备更换其相应的图像模版
						judge
						#检查是否有这个网卡的图象
						if_eth0=`${PHP} ${GRAPHS} --host-id=${hostid} --snmp-field=ifDescr --list-snmp-values | grep "$interface"`
						#if_bond0=`${PHP} ${GRAPHS} --host-id=${hostid} --snmp-field=ifDescr --list-snmp-values | grep "bond0"`
						if [ "x${if_eth0}" != "x"  ];then
							re=`new_graph ${inet_graph_tid} ${if_eth0}`
							Log "$LINENO: interface: $interface|  new_graph $re"
						else
							Log "$LINENO 已经有interface: $interface 接口的图象"
						fi
						update_dg
						Log "$LINENO update_dg   This is a null device, creating a map -- $ip $hostname"
					fi
					Log  "$LINENO: This $ip $hostname is already exists"
			fi
		fi

	else
        Log "$LINENO ${ip} $hostname -- No Response Device" 
fi
done <$ips
}


addinet
#change_gr
update_allg
